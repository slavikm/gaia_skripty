#!/bin/bash
#
# skript pro odmazavani souboru (zachova poslednich X souboru)
#
#
# msl@actinet.cz

# konfigurace
FILES_DIR=/var/backups
HOW_MANY_TO_LEAVE=14
FILE_PATTERN=backup_
FILE_SUFFIX= # muze byt prazdny


ponech_x_souboru() {

        echo "mslbck: ponechavam poslednich ${HOW_MANY_TO_LEAVE} souboru (${FILE_PATTERN})"
        # z duvodu globingu nutno spustit subshell
        rm -f $(ls -t ${FILES_DIR}/${FILE_PATTERN}*${FILE_SUFFIX} | awk "NR>${HOW_MANY_TO_LEAVE}")
}

ponech_x_souboru
