#!/bin/bash
# ---------------------------------------------------------------------------
# jmeno:    CParchivelogs.sh                                                #
# ucel:     archivace logu CheckPoint managementu                           #
# autor:    msl (slavikm@gmail.com), 2012                                   #
#                                                                           #
# pozn: pro kontrolu disku predpoklada nastaveny mail-notification:         #
#   set set mail-notification server 1.2.3.4                                #
#   set mail-notification username from@example.com                         #
#   (kontrola v /etc/msmtp.conf)                                            #
#                                                                           #
# 2012-07-18 - msl v0.1 - udrzba logu s kontrolou disku na gaia R75.40      #
# 2012-10-18 - msl v0.2 - pridan lokalni konfig, je vhodne linkovat do /etc #
#    z aktualniho adresare.                                                 #
# 2012-10-18 - msl v0.2 - soubor s mailem se vytvari jako temporary         #
# 2012-10-19 - msl v0.3 - uprava syntaxe, pridan remote backup logu         #
# 2012-11-02 - msl v1 - otestovano, kosmeticke upravy                       #
#                                                                           #
# ---------------------------------------------------------------------------

# =======================================================================
# = Lokalni promenne (je mozne je nacist z /etc/CParchivelogs.cfg)      =
# =   obykle jsou do etc linkovany:                                     =
# = ln -s /home/admin/service/CParchivelogs.cfg /etc/CParchivelogs.cfg  =
# =======================================================================


ARCHIVEAFTER=30             # po jake dobe komprimovat logy? (days)
OLDLOGS=/var/log/oldfwlogs  # uloziste starych logu
DLTAFTER=180                # po jake dobe mazat logy z $OLDLOGS? (days)

# archivujeme vzdalene pres scp?
REMOTEARCHIV=ne             # ano/ne
REMOTESRV=                  # user@IP.ADD.RE.SS
REMOTEDIR=                  # cilovy adresar
REMOTEKEY=                  # ssh klic

## pro kontrolu mista na disku - GAIA only! (promenne)
KONTROLUJDISK=ne        # kontrolovat disk (ano/ne)
OD=fwm@example.com      # pole "From:" mailu
KOMU="msl@example.cz"   # mozno i vice mailu oddelenych carkami
PARTISNA=/var/log       # jmeno kontrolovane partisny
DISKMAX=90              # v procentech, pri presahnuti posle mail

# ==================================================
# = pomocne promenne (nemenit nic za timto radkem) =
# ==================================================

# nacteni (vyse uvedenych) promennych z lokalniho konfigu
if [[ -e  /etc/CParchivelogs.cfg ]]; then
source /etc/CParchivelogs.cfg
fi


# nacteni promennych CheckPointu
source /etc/profile.d/CP.sh


# ======================
# = PRIPRAVA PROSTREDI =
# ======================

# kontrola existence $OLDLOGS adresare
if [[ ! -e  ${OLDLOGS} ]]; then
	mkdir ${OLDLOGS}
fi

# vytvoreni linku $FWDIR/log/old (pokud neexistuje)
if [[ ! -e  ${FWDIR}/log/old ]]; then
	ln -s ${OLDLOGS} ${FWDIR}/log/old
fi

# ==================
# = KONTROLA DISKU =
# ==================

function poslimail {
	# naformatuje mail do souboru a ten pak posle pomoci msmtp
	TMPMAIL=`mktemp /var/tmp/ma.XXXXXX`
cat << EOF > ${TMPMAIL}
To: ${KOMU}
From: ${OD}
Subject: nedostatek mista na disku stroje `hostname`

Problem:
------------------------------------------------------------------

Disk ${PARTISNA} je zaplnen vice nez z ${DISKMAX}%.

Aktualni obsazenost je ${DISKSIZE}%.


celkova obsazenost (df -h):
------------------------------------------------------------------

`df -HP | grep -vE "^Filesystem|tmpfs" |awk '{print $5 " (obsazeno " $3 " z " $2 ")\t" $6 }'`

poznamka: kontrola mista se provadi pred komprimaci logu, skutecne 
          volne misto muze byt o neco jine (vetsi). 

EOF

###debug echo "cat ${TMPMAIL} |msmtp  -C /etc/msmtp.conf -t"
	cat ${TMPMAIL} |msmtp  -C /etc/msmtp.conf -t
	rm ${TMPMAIL}
}


function checkpartsize {
        DISKSIZE=`df -HP $1 | sed '1d' | awk '{print $5}' | cut -d'%' -f1`
        ###debug echo "DISKSIZE je ${DISKSIZE}"
        if [[ "${DISKSIZE}" -gt "${DISKMAX}" ]]; then
                ### debug echo "obsazenost ${PARTISNA} (${DISKSIZE}%) presahla nastavenych ${DISKMAX}%"
                poslimail
        fi
}

# ============================
# = provedeni kontroly disku =
# ============================

if [[  "${KONTROLUJDISK}" == "ano" ]]; then
	checkpartsize "${PARTISNA}"
fi

# =======================
# = VLASTNI UDRZBA LOGU =
# =======================

if [ -e ${FWDIR}/log -a -e ${FWDIR}/log/old ]; then
	# provedeme archivaci (nejdrive zkomprimujeme v log adresari)
	/usr/bin/find ${FWDIR}/log/ -maxdepth 1 -type f -name "*log*" -mtime +${ARCHIVEAFTER} -exec /usr/bin/gzip -f {} \;
	# remote archivace a presun do old, nebo presun do old
	#
	if [[ "${REMOTEARCHIV}" == "ano" ]]; then
		#odkopirujeme s kontrolou, ze probehlo a pak odsuneme do old
		if  $(/usr/bin/find ${FWDIR}/log/ -maxdepth 1 -type f -name "*.gz" -exec scp -i ${REMOTEKEY} {} ${REMOTESRV}:${REMOTEDIR} \; ); then
			# scp je OK, mazeme gz soubory
			/usr/bin/find ${FWDIR}/log/ -maxdepth 1 -type f -name "*.gz" -exec /bin/rm -f {} \;
		else
			echo "neprobehla vzdalena zaloha, nic nemazu"
		fi
	else
		# pokud neni remote archiv, pouze odsuneme do old
		/usr/bin/find ${FWDIR}/log/ -maxdepth 1 -type f -name "*.gz" -exec /bin/mv -f {} ${FWDIR}/log/old/ \;
		# a uklidime
		/usr/bin/find ${FWDIR}/log/old/ -maxdepth 1 -type f -mtime +${DLTAFTER} -name "*.gz" -exec /bin/rm -f {} \;	
	fi
fi

# =========================
# = uvolneni blobu (pcap) =
# =========================
/usr/bin/find $FWDIR/log/blob/ -mtime +30 -delete
