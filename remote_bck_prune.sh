#!/bin/bash

if [[ -e  /etc/CPbackup.cfg ]]; then
source /etc/CPbackup.cfg
fi

ADRESAR=$(dirname "$0")
cd $ADRESAR

 scp -i ${REMOTEKEY} ${PRUNESCRIPT} ${REMOTESRV}:prunescript.sh
 ssh -i ${REMOTEKEY} ${REMOTESRV} "./prunescript.sh" ${REMOTEKEEPAMOUNT} ${REMOTEDIR}
