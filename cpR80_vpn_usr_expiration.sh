#!/bin/bash
# ****************************************************************************
#   jmeno:    cp_vpn_usr_expiration                                          *
#   ucel:     posle mail alert vpn uzivatelum s upozornenim na expiraci uctu *
#   autor:    msl (slavikm@gmail.com), 2015                                  *
#                                                                            *
#   pozn: pro zasilani mailu predpoklada nastaveny mail-notification         *
#         v gaia systemu:                                                    *
#     set set mail-notification server 1.2.3.4                               *
#     set mail-notification username from@example.com                        *
#     (kontrola v /etc/msmtp.conf)                                           *
#                                                                            *
#                                                                            *
#  2015-10-23 - msl v 0.1 - zakladni funkcionalita                           *
#  2018-03-16 - msl v 0.2 - prepsano pro R80, ktera nema fwm dbexport        *
#                           data ziskany z dbeditu                           *
#  2019-05-23 - msl v 0.3 - pridano druhe varovani v den expirace            *
#                                                                            *
#                                                                            *
# ****************************************************************************


# ****************************************************************************
# *                             lokalni promenne                             *
# ****************************************************************************

ZAKAZNIK='CUSTOMER'
EXPIREDAYS='31'
OD='skript@example.cz'
##DEBUG## EXPIRY_DATE='30-Jun-2017'
EXPIRY_DATE=`date -d "$EXPIREDAYS days" +%d-%b-%Y`
TODAY=`date +%d-%b-%Y`
ADMINMAIL='some@admnin.mail'  # vice adminu oddeleno carkami
SCREENONLY='0' # mail debug
ADMINCOPY='1' # poslat uzivatelsky mail i na admina?
ADMINONLY='0' # nezasila uzivateli, pouze adminovi
BCCMAIL='' # bcc alert

# ****************************************************************************
# *                                  funkce                                  *
# ****************************************************************************



function PosliMail () {
    TMPMAIL=`mktemp /var/tmp/ma.XXXXXX`
    if [[ "$1" == "today" ]]; then
	SUBJECT="Subject: upozorneni - konec platnosti vaseho VPN uctu"
	KONCI="dnes vyprsela"
    else
	SUBJECT="Subject: upozorneni - blizi se konec platnosti vaseho VPN uctu"
	KONCI="konci za ${EXPIREDAYS} dni"
    fi

cat << EOF > ${TMPMAIL}
To: ${KOMU}
Bcc: $BCCMAIL
From: ${OD}
${SUBJECT}

Dobry den.
Platnost VPN uctu "$CLOVEK" ($USERMAIL) pro vzdaleny pristup do ICT systemu $ZAKAZNIK ${KONCI}.
Pozadejte garanta smlouvy o prodlouzeni platnosti uctu.

S pozdravem oddeleni IT


(mail je generovan skriptem, neodpovidejte na nej)

EOF

    if [[ ${SCREENONLY} -eq '1' ]]; then
        echo "" # neposilam mail, pouze vypis na obrazovku (DEBUG)
        echo ""
        cat ${TMPMAIL}
    else
        cat ${TMPMAIL} |msmtp  -C /etc/msmtp.conf -t
    fi
    rm ${TMPMAIL}
}

# nacteni uzivatelu s blizkou expiraci
function userDataToFile {
    source /etc/profile.d/CP.sh
    USRDATAFILE=`mktemp /var/tmp/usrdat.XXXXXX`
    echo -e "query users, expiration_date=\"${EXPIRY_DATE}\"\n-q\n" | dbedit -local | grep -e "Object\ Name:\|email" > $USRDATAFILE
    echo $USRDATAFILE
}

# totez pro dnes expirovane ucty
function userDataToFile2 {
    source /etc/profile.d/CP.sh
    USRDATAFILE=`mktemp /var/tmp/usrdatTODAY.XXXXXX`
    echo -e "query users, expiration_date=\"${TODAY}\"\n-q\n" | dbedit -local | grep -e "Object\ Name:\|email" > $USRDATAFILE
    echo $USRDATAFILE
}

# ****************************************************************************
# *                                nacteni dat                               *
# ****************************************************************************

source /etc/profile.d/CP.sh
# expirace za X dni
DATA=$(userDataToFile)
# expirace dnes
DATA_TODAY=$(userDataToFile2)


# ****************************************************************************
# *                             hlavni procedura                             *
# ****************************************************************************

# vysekame z dbedit struktury usernames
UZIVATELE=`cat ${DATA} | grep Name | sed 's/Object Name: //' | tr '\n' ' '`
UZIVATELE_TODAY=`cat ${DATA_TODAY} | grep Name | sed 's/Object Name: //' | tr '\n' ' '`

# pro kazdeho uzivatele ziskame jeho mail a posleme mu upozorneni

for CLOVEK in $UZIVATELE; do
    email=`egrep -A1 "Object Name:.*${CLOVEK}" $DATA | sed '/Object Name/d' | awk {'print $2;'}`
    USERMAIL=$(echo $email) # echo zbavi mezer
    [ -z $email ] && continue #skip account with empty emails
    if [ ${ADMINONLY} -eq '1' ]; then
        KOMU="${ADMINMAIL}"
    elif [ ${ADMINCOPY} -eq '1' ]; then
        KOMU="${USERMAIL}, ${ADMINMAIL}"
    else
        KOMU="${USERMAIL}"
    fi
    PosliMail
done

# a jeste jednou pro dnes expirovane ucty

for CLOVEK in $UZIVATELE_TODAY; do
    email=`egrep -A1 "Object Name:.*${CLOVEK}" $DATA_TODAY | sed '/Object Name/d' | awk {'print $2;'}`
    USERMAIL=$(echo $email) # echo zbavi mezer
    [ -z $email ] && continue #skip account with empty emails
    if [ ${ADMINONLY} -eq '1' ]; then
        KOMU="${ADMINMAIL}"
    elif [ ${ADMINCOPY} -eq '1' ]; then
        KOMU="${USERMAIL}, ${ADMINMAIL}"
    else
        KOMU="${USERMAIL}"
    fi
    PosliMail today
done

# uklid tmp souboru s daty

rm ${DATA}
rm ${DATA_TODAY}

