#!/bin/bash
# ---------------------------------------------------------------------------
# jmeno:    CPbackup-gaia.sh                                                #
# ucel:     backup CheckPoint managementu                                   #
# autor:    msl (slavikm@gmail.com), 2012                                   #
#                                                                           #
# pozn: pro kontrolu disku predpoklada nastaveny mail-notification:         #
#   set set mail-notification server 1.2.3.4                                #
#   set mail-notification username from@example.com                         #
#   (kontrola v /etc/msmtp.conf)                                            #
#                                                                           #
#   Zaloha CheckPoint managementu pocita s temito scenari:                  #
#                                                                           #
#   primarni management                                                     #
#   ===================                                                     #
#   - provede zalohu CP DB pomoci migrate export                            #
#   - provede zalohu lokanich souboru (skripty, etc)                        #
#   - posle kopii zaloh na remote stroj (existuje-li)                       #
#   - provede udrzbu starych zaloh (odmaze)                                 #
#                                                                           #
#   sekundarni management                                                   #
#   =====================                                                   #
#   - jako primar, s vyjimkou remote kopie                                  #
#                                                                           #
#   bonus funkce                                                            #
#   ===================                                                     #
#   - pouze kontrola disku s reportem problemu mailem                       #
#                                                                           #
# 2012-10-31 - msl v 1.0 - init verze, provedeny testy                      #
# 2012-11-05 - msl v 1.1 - pridana funkce testu mailu, oprava kontr. disku  #
# 2013-04-12 - msl v 1.2 - opraveno odmazávání starých záloh                #
# 2013-09-18 - msl v 1.3 - uprava zalohy etc, opravena funkce kontr. disku  #
# TODO: dodelat kontrolu remote uloziste                                    #
#                                                                           #
# ---------------------------------------------------------------------------


# =============================================================
# = Lokalni promenne (je mozne je nacist z /etc/CPbackup.cfg) =
# =   obykle jsou do etc linkovany:                           =
# = ln -s /home/admin/service/CPbackup.cfg /etc/CPbackup.cfg  =
# =============================================================


BCK_BINPATH=/home/admin/service/ # umisteni tohoto skriptu
BCK_TMPROOT=/var/log/tmp/   # umisteni docasnych adresaru
                            # u Gaia ve /var/log/ a pak 
                            # dynamicky skriptem
BCK_DESTDIR=/var/log/CPbackup/backups # kam ukladame zalohy
DLTAFTER=14 # po jake dobe mazat zalohy z $BCK_DESTDIR? (days)

# zalohujeme vzdalene pres scp?
REMOTEARCHIV=ne             # ano/ne
REMOTESRV=                  # user@IP.ADD.RE.SS
REMOTEDIR=                  # cilovy adresar
REMOTEKEY=                  # ssh klic
#
# remote backup cleanup variables
# nacitano skriptem remote_bck_prune.sh z cronu
REMOTEKEEPAMOUNT=15     # zanechat poslednich X souboru
PRUNESCRIPT=/home/admin/service/keep-last-x-files.sh
#
## pro kontrolu mista na disku (promenne)
KONTROLUJDISK=ne        # kontrolovat disk (ano/ne)
OD=fwm@example.com      # pole "From:" mailu
KOMU="msl@example.cz"   # mozno i vice mailu oddelenych carkami
DISKY=( /var/log / )    # jmena kontrolovanych disku (partisen)
DISKMAX=90              # v procentech, pri prekroceni posle mail

ZALOHUJ_SOUBORY=ano     # zalohovat lokalni soubory (ano/ne)
BCK_FILES=( /etc/ /home/admin/service/ /config/ ) # co zalohujeme


# ==================================================
# = pomocne promenne (nemenit nic za timto radkem) =
# ==================================================


# nacteni (vyse uvedenych) promennych z lokalniho konfigu
if [[ -e  /etc/CPbackup.cfg ]]; then
source /etc/CPbackup.cfg
fi

CALLER=`basename $0`         # The Caller name
let "errorCounter = 0"		 # Priprava pro debug
CHYBOVAZPRAVA=""			 # Priprava pro debug
CURRENT_DATE=`/bin/date +%Y%m%d-%H%M` # datum pro nazev
HOSTNAME=`hostname`
BCK_CP_FILENAME="${CURRENT_DATE}_mgmt_${HOSTNAME}.tgz" # nazev CP migrate zalohy
BCK_SVC_FILENAME="${CURRENT_DATE}_servicefiles_${HOSTNAME}.tgz" # nazev svc files zalohy


# nacteni promennych CheckPointu
if [[ -e /etc/profile.d/CP.sh ]]; then
	source /etc/profile.d/CP.sh
else
	let "errorCounter = errorCounter + 1"
	CHYBOVAZPRAVA+=" - nemohu nacist CP profile\n"
fi


# =====================================
# = procedura vypsani pouziti skriptu =
# =====================================

usage()
{
 echo ""
 echo "POUZITI: $CALLER [pri|sec|df|mailtest]"
 echo ""
 echo "PARAMETR:  pri = primary mgt backup"
 echo "           sec = secondary mgt backup"
 echo "           df = disk check only"
 echo "           mailtest = posle testovaci mail (test nastaveni a fce)"
 echo ""
 echo "PREREQUISITES:"
 echo "* Pro kontrolu disku a report chyb je nutne mit nastaven"
 echo "  mail subsystem pres mstmp"
# echo "$CALLER: exiting now with rc=1."
 exit 1
}

# =========================
# = funkce kontroly disku =
# =========================
# 
# - bere jako parametr partisnu
# - vola poslimail a predava ji jmeno partisny (pro iterace)

function CheckPartSize {
        DISKSIZE=`df -HP $1 | sed '1d' | awk '{print $5}' | cut -d'%' -f1`
        ###debug echo "DISKSIZE je ${DISKSIZE}"
        if [[ "${DISKSIZE}" -gt "${DISKMAX}" ]]; then
                ### debug echo "obsazenost ${PARTISNA} (${DISKSIZE}%) presahla nastavenych ${DISKMAX}%"
                PosliMail ${1}
		else
			if [[ -n ${SCREENONLY} ]]; then # pri interaktivnim volani neco vypiseme
			echo ""
			echo "zaplneni disku ${1} (${DISKSIZE}%) nepresahlo nastavenou hodnotu ${DISKMAX}%"
			fi
        fi
}

function CheckAllPartitions {
	if [[ ${#DISKY[@]} > 0 ]]; then
		echo "pocet kontrolovanych partisen: ${#DISKY[@]}"
		for myDISK in "${DISKY[@]}"
		do
			#echo "${myDISK}"
			CheckPartSize "${myDISK}"
		done
	fi
}

# =========================
# = funkce odeslani mailu =
# =========================

function PosliMail {
	# naformatuje mail do souboru a ten pak posle pomoci msmtp
	TMPMAIL=`mktemp /var/tmp/ma.XXXXXX`
cat << EOF > ${TMPMAIL}
To: ${KOMU}
From: ${OD}
Subject: nedostatek mista na disku stroje `hostname`

:===================:
|  popis problemu:  
:===================:

Disk ${1} je zaplnen vice nez z ${DISKMAX}%.

Aktualni obsazenost je ${DISKSIZE}%!!!


:=============================:
| celkova obsazenost (df -h): 
:=============================:

`df -HP | grep -vE "^Filesystem|tmpfs" |awk '{print $5 " (obsazeno " $3 " z " $2 ")\t" $6 }'`

EOF

if [[ -n ${SCREENONLY} ]]; then
	echo ""    # neposilam mail, pouze vypis na obrazovku
	echo ""
	cat ${TMPMAIL}
else
	cat ${TMPMAIL} |msmtp  -C /etc/msmtp.conf -t
fi
	rm ${TMPMAIL}
}
#
# konec funkce PosliMail
# ====================================

# ========================================
# = Funkce pro testovani nastaveni mailu =
# ========================================

function OtestujMail {
	# naformatuje mail do souboru a ten pak posle pomoci msmtp
	TMPMAIL=`mktemp /var/tmp/ma.XXXXXX`
cat << EOF > ${TMPMAIL}
To: ${KOMU}
From: ${OD}
Subject: test nastaveni mailu na stroji `hostname`

:===================:
|  test MAILU:       
:===================:


testuji nastaveni mailu (v /etc/msmtp.conf) v OS GAIA.

Zda se, ze je vse OK.

S uctou vas skript :)

EOF

if [[ -n ${SCREENONLY} ]]; then
	echo "" # neposilam mail, pouze vypis na obrazovku (DEBUG)
	echo ""
	cat ${TMPMAIL}
else
	cat ${TMPMAIL} |msmtp  -C /etc/msmtp.conf -t
fi
	rm ${TMPMAIL}
}



# ==========================================
# = procedura vytvoreni docasneho adresare =
# ==========================================

pripravtmp()
{
	# vytvorime temp adresar, pokud neexistuje
	if [[ ! -e ${BCK_TMPROOT} ]]; then
		echo "vytvarim tmproot"
		mkdir -p ${BCK_TMPROOT}
	fi
	# samotny adresar pro bck data
	BCK_TMPDIR=`mktemp -d ${BCK_TMPROOT}/CPbck.XXXX`
}

uklidtmp()
{   # smazem jen neprazdnou promennou
	if [[ ! -z {BCK_TMPDIR}$ ]]; then
		echo "mazu ${BCK_TMPDIR}"
		rm -rf ${BCK_TMPDIR}
	fi
}

uklid_stare_zalohy()
{ # smazema zalohy starsi nez $DLTAFTER (mame li nastaveno)
	if [[ ! -z ${DLTAFTER} ]]; then
##		echo "uklizim stare zalohy" #fordebug (zakomentovat)
		find ${BCK_DESTDIR}/ -maxdepth 1 -type f -name "*.tgz" -mtime +${DLTAFTER} -exec rm -r {} \;
	fi
}

zalohuj_soubory()
{ # zaloha souboru
if [ "${ZALOHUJ_SOUBORY}" != "ne" ]; then
	cd ${BCK_BINPATH}
	if tar czf ${BCK_DESTDIR}/${BCK_SVC_FILENAME} --exclude="emergendisk" ${BCK_FILES[@]} ; then
		# zaloha probehla ok
		echo "${CURRENT_DATE} - zaloha souboru OK" >> CPbackup.log
		if [  "${REMOTEARCHIV}" != "ne" ]; then
		  ### fordebug (zakomentovat radek nize)
###	      echo "scp -i ${REMOTEKEY} ${BCK_DESTDIR}/${BCK_SVC_FILENAME} ${REMOTESRV}:${REMOTEDIR}/"
	      if scp -o IdentitiesOnly=yes  -i ${REMOTEKEY} ${BCK_DESTDIR}/${BCK_SVC_FILENAME} ${REMOTESRV}:${REMOTEDIR}/; then
	        echo "${BCK_SVC_FILENAME} - Remote copy of OK" >> CPbackup.log
	      else
		  	let "errorCounter = errorCounter + 1"
		  	CHYBOVAZPRAVA+="${BCK_SVC_FILENAME} - nemohu zkopirovat do ${REMOTESRV}!!!\n"
	      fi
		fi
	else
	  	let "errorCounter = errorCounter + 1"
	  	CHYBOVAZPRAVA+="${CURRENT_DATE} - nemohu zazalohovat lokalni soubory!!!\n"
	fi
fi
}

zalohujCP()
{
	cd ${FWDIR}/bin/upgrade_tools
	if echo "\r" | ./migrate export -n  ${BCK_TMPDIR}/${BCK_CP_FILENAME} &>/dev/null; then
		if cd ${BCK_BINPATH}; then
			chmod 660 ${BCK_TMPDIR}/${BCK_CP_FILENAME}
			# zkopirujeme do remote uloziste
			if [  "${REMOTEARCHIV}" != "ne" ]; then
				### fordebug (zakomentovat radek nize)
###				echo "scp -i ${REMOTEKEY} ${BCK_TMPDIR}/${BCK_CP_FILENAME} ${REMOTESRV}:${REMOTEDIR}/"
				if scp -o IdentitiesOnly=yes -i ${REMOTEKEY} ${BCK_TMPDIR}/${BCK_CP_FILENAME} ${REMOTESRV}:${REMOTEDIR}/; then
					echo "${CURRENT_DATE} - Remote copy OK" >> CPbackup.log
				else
					let "errorCounter = errorCounter + 1"
					CHYBOVAZPRAVA+="${CURRENT_DATE} - nemohu zkopirovat ${BCK_TMPDIR}/${BCK_CP_FILENAME}!!!\n"
				fi
			fi
			# presuneme do ciloveho lokalniho adresare
			if mv ${BCK_TMPDIR}/${BCK_CP_FILENAME} ${BCK_DESTDIR}; then
				echo "${CURRENT_DATE} - ${BCK_TMPDIR}/${BCK_CP_FILENAME} Moved to ${BCK_DESTDIR} OK" >> CPbackup.log
			else
				let "errorCounter = errorCounter + 1"
				CHYBOVAZPRAVA+="{$CURRENT_DATE} - nemohu presunout ${BCK_TMPDIR}/${BCK_CP_FILENAME} do ${BCK_DESTDIR}!!!\n"
			fi
		fi
	else
		let "errorCounter = errorCounter + 1"
		CHYBOVAZPRAVA+="{$CURRENT_DATE} - nemohu udelat mograte export!!!\n"
	fi
}

### fordebug (zakomentovat)
### exit 0

# ====================================
# = hlavni procedura (dle parametru) =
# ====================================

case $1 in
	-h|--help)
	usage
	;;
	-p|pri)
###	echo "primary" # fordebug (zakomentovat)
	#zkontrolujem disk
	if [[  "$KONTROLUJDISK" == "ano" ]]; then
		CheckAllPartitions
	fi
		pripravtmp
			zalohujCP
			zalohuj_soubory
			uklid_stare_zalohy
		uklidtmp
	;;
	-s|sec)
###	echo "sekundary" #fordebug (zakomentovat)
	#zkontrolujem disk
	if [[  "$KONTROLUJDISK" == "ano" ]]; then
		CheckAllPartitions
	fi
	REMOTEARCHIV=ne	# na sekundaru nechceme remote kopii
		pripravtmp
			zalohujCP
			zalohuj_soubory
			uklid_stare_zalohy
		uklidtmp
	;;
	-d|df)
	echo "kontrola disku"
	SCREENONLY='1'
	CheckAllPartitions
	;;
	-m|mailtest)
	#SCREENONLY='1' # debug only
	OtestujMail
	;;
    *)
      echo "nespravna volba: $1"; echo "" >&2
	  usage
      exit 1
      ;;
esac
	
### debuf off - vypnuto vypisovani chyb
exit 0
	
# -------------------------------
# vypsani chyb
# -------------------------------

if [[ ${errorCounter} -ne 0 ]]; then
	echo ""
	echo "# -----------------"
	echo "# CHYBOVA HLASENI: "
	echo "# -----------------"
	echo ""
	echo "${CURRENT_DATE}"
	echo ""
	echo "Pocet chyb: ${errorCounter}:"
	echo "-----------------"
	printf "${CHYBOVAZPRAVA}"
fi

echo ""
echo "exit z `basename $0`" 
echo ""

exit 0
