#!/bin/bash

STROJE="pat mat jaja paja"
datum=$(/bin/date +%Y%m%d);

function vypis_verze() {
	for stroj in $STROJE; do
		ssh admin@${stroj}  'echo -n "==========="; hostname; clish -c "show version all"; fw ver; cpinfo -y all | grep JUMBO | sort | uniq'
		echo
	done
}

function status() {
	for stroj in $STROJE; do
		echo "---------------${stroj}--------------------------"
		ssh admin@${stroj}  'hostname; cphaprob stat; w; echo; df -h; echo; free'
		echo
	done
}

function spust_zalohu() {
	for stroj in $STROJE; do
		ssh admin@${stroj}  'clish -c "add backup local"'
		echo
	done
}


function listuj_zalohu() {
	for stroj in $STROJE; do
		ssh admin@${stroj}  'ls -l /var/log/CPbackup/backups/backup*'
		echo
	done
}

function smaz_zalohu() {
	for stroj in $STROJE; do
		ssh admin@${stroj}  'rm -f /var/log/CPbackup/backups/backup*'
		echo
	done
}

function kopiruj_zalohu() {
	for stroj in $STROJE; do
		scp admin@${stroj}:/var/log/CPbackup/backups/backup* .
		echo
	done
}


function get_config() {
	for hostname in $STROJE; do ssh actinet@$hostname "clish -c \"show configuration\"" | tee ${datum}_${hostname}_config  ; done
}



case $1 in
	ver)
	vypis_verze
	;;
	bck)
	spust_zalohu
	;;
	cp)
	kopiruj_zalohu
	;;
	ls)
	listuj_zalohu
	;;
	rm)
	smaz_zalohu
	;;
	st)
	status
	;;
	conf)
	get_config
	;;
	*)
	echo "jen parametry ver,bck,cp,ls,rm,st,conf"
	;;
esac
