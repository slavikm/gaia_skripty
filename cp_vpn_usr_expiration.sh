#!/bin/bash
# ****************************************************************************
#   jmeno:    cp_vpn_usr_expiration                                          *
#   ucel:     posle mail alert vpn uzivatelum s upozornenim na expiraci uctu *
#   autor:    msl (slavikm@gmail.com), 2015                                  *
#                                                                            *
#   pozn: pro zasilani mailu predpoklada nastaveny mail-notification         *
#         v gaia systemu:                                                    *
#     set set mail-notification server 1.2.3.4                               *
#     set mail-notification username from@example.com                        *
#     (kontrola v /etc/msmtp.conf)                                           *
#                                                                            *
#                                                                            *
#  2015-10-23 - msl v 0.1 - zakladni funkcionalita                           *
#                                                                            *
#                                                                            *
# ****************************************************************************


# ****************************************************************************
# *                             lokalni promenne                             *
# ****************************************************************************

datafile="/var/tmp/users.txt" # musi byt full path
dnes=`date +%Y-%m-%d`
ZAKAZNIK='CUSTOMER'
EXPIREDAYS='31'
OD='skript@example.com'
ADMINMAIL='fwadmin@example.com'  # vice adminu oddeleno carkami
SCREENONLY='0' # mail debug
ADMINCOPY='1' # poslat uzivatelsky mail i na admina?
ADMINONLY='1' # nezasila uzivateli, pouze adminovi
BCCMAIL='' # bcc alert

# ****************************************************************************
# *                                  funkce                                  *
# ****************************************************************************

function date2stamp {
    date -d "$1" +%s
}

function dateDiff {
    case $1 in
        -s)   sec=1;      shift;;
        -m)   sec=60;     shift;;
        -h)   sec=3600;   shift;;
        -d)   sec=86400;  shift;;
        *)    sec=86400;;
    esac
    dte1=$(date2stamp $1)
    dte2=$(date2stamp $2)
    diffSec=$((dte2-dte1))
    #if ((diffSec < 0)); then abs=-1; else abs=1; fi
    # echo $((diffSec/sec*abs))
    echo $((diffSec/sec))
}


function PosliMail {
    TMPMAIL=`mktemp /var/tmp/ma.XXXXXX`
cat << EOF > ${TMPMAIL}
To: ${KOMU}
Bcc: $BCCMAIL
From: ${OD}
Subject: upozorneni - blizi se konec platnosti vaseho VPN uctu

Dobry den.
Platnost VPN uctu "$name" ($USERMAIL) pro vzdaleny pristup do ICT systemu $ZAKAZNIK konci za $EXPIREDAYS dni.
Pozadejte garanta smlouvy o prodlouzeni platnosti uctu.

S pozdravem oddeleni IT


(mail je generovan skriptem, neodpovidejte na nej)

EOF

    if [[ ${SCREENONLY} -eq '1' ]]; then
        echo "" # neposilam mail, pouze vypis na obrazovku (DEBUG)
        echo ""
        cat ${TMPMAIL}
    else
        cat ${TMPMAIL} |msmtp  -C /etc/msmtp.conf -t
    fi
    rm ${TMPMAIL}
}

# ****************************************************************************
# *                                nacteni dat                               *
# ****************************************************************************

source /etc/profile.d/CP.sh
fwm dbexport -a {name,groups,expiration_date,email,comments} -f $datafile


# ****************************************************************************
# *                             hlavni procedura                             *
# ****************************************************************************

while IFS=';' read name groups expiration_date email comment zbytek ; do
[ $name == 'name' ] && continue #header skip
[ -z $email ] && continue #skip account with empty emails
timespan=$(dateDiff -d $dnes $expiration_date)
if [ $timespan -eq $EXPIREDAYS ] ; then
#     echo $expiration_date $timespan
    USERMAIL=$(echo $email) # echo zbavi mezer
    if [ ${ADMINONLY} -eq '1' ]; then
        KOMU="${ADMINMAIL}"
    elif [ ${ADMINCOPY} -eq '1' ]; then
        KOMU="${USERMAIL}, ${ADMINMAIL}"
    else
        KOMU="${USERMAIL}"
    fi
    PosliMail
fi
done < $datafile
