#!/bin/bash
#
# skript pro odmazavani souboru (zachova poslednich X souboru)
# remote spusteni ze zalohovaneho mgmt
#
# msl@actinet.cz

# konfigurace
FILES_DIR=$2
HOW_MANY_TO_LEAVE=$1
FILE_PATTERN1="_mgmt_"
FILE_PATTERN2="_servicefiles_"
FILE_SUFFIX= # muze byt prazdny


ponech_x_souboru () {
	local FILE_PATTERN=$1
	local AMOUNT=${HOW_MANY_TO_LEAVE}
        echo "mslbck: ponechavam poslednich ${HOW_MANY_TO_LEAVE} souboru (${FILE_PATTERN}) v ${FILES_DIR}"
        # z duvodu globingu nutno spustit subshell
        rm -f $(ls -t ${FILES_DIR}/*${FILE_PATTERN}*${FILE_SUFFIX} | awk "NR>${HOW_MANY_TO_LEAVE}")
}

ponech_x_souboru ${FILE_PATTERN1}

ponech_x_souboru ${FILE_PATTERN2}

